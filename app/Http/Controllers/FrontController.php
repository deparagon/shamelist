<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscriber;
use Tools;
class FrontController extends Controller
{


    public function index()
    {
        
    	return view('front.soon', ['pg'=>'soon']);
    }

   public function home()
    {
        
      return view('front.home', ['pg'=>'soon']);
    }

    public function login()
    {
    	return view('front.login', ['pg'=>'login']);
    }

    public function register()
    {
    	       
        return view('front.register',  ['pg'=>'register']);
    }

       public function terms()
    {
       
      return view('front.terms' , ['pg'=>'terms']);
    }

   public function privacy()
    {
       
      return view('front.privacy' , ['pg'=>'privacy']);
    }



    public function contactus()
    {
       
    	return view('front.contactus' , ['pg'=>'contactus']);
    }

    
    public function contactSave(Request $request)
    {
         $this->validate($request, ['fullname' => 'required', 'email' =>'required|email', 'message' => 'required']);

      Mail::to('info@shamelist.com.ng')->send( new ContactMessage( $request->fullname, $request->email, $request->phone, $request->subject, $request->message));
      

         return back()->with('successm', 'Message sent successfully, We will contact you soonest');
    }


    public function faq()
    {
         $allfaq = Faq::getPublisedFaq();
        
        return view('front.faq', ['allfaq' =>$allfaq, 'pg'=>'faq']);
    }


   public function aboutus()
    {
         
        return view('front.aboutus' , ['pg'=>'aboutus']);
    }


    public function recoverPass()
    {
         
        return view('front.password', ['pg'=>'recovery']);
    }

        public function resetPass(Request $request)
     {
        $this->validate($request, ['phone' => 'required']);

        $member = User::where(['phone' => $request->phone])->first();
   if(is_object($member)){
    $newpass = Tools::codeGenerator(8);

            User::changePassword($member->id, $newpass);

         Mail::to($member->email)->send(new NewPasswordMail($member, $newpass));

         return back()->with(['successm' => 'Temporary password has been sent to your Email']);
   }
        else{
               return back()->with(['errorm' => 'Invalid Phone number provided']);
        }

        return back()->with(['errorm' => 'Invalid Phone provided']);

     }





    public function subscribe(Request $req)
   {
        $email = $req->email;
        if(!Tools::validEmail($email)){
           Tools::naError('Enter a valid email');
           exit;
        }
        $subc = new Subscriber();

        if($subc->isHere($email)){
          Tools::naInfo('Already Subscribed to ShameList');
          exit;
        }

        $subc->addSub($email);

        Tools::naSuccess('Subscription completed successfully');
        exit;
         

   }
      public function blank()
      {
        
      }




}
