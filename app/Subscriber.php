<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    //

    public  function addSub($email)
    {
         $s = new self();
         $s->email = $email;
         $s->save();
         return $s;
    }

    public function isHere($email)
    {
      $data = self::where(['email'=>$email])->first();
      if(is_object($data)){
      	return true;
      }
      return false;
    }
}
