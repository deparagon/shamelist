<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', 'FrontController@index');
Route::post('/', 'FrontController@blank');
Route::get('/home', 'FrontController@home');
Route::get('/login', 'FrontController@login');
Route::get('/register', 'FrontController@register');
Route::get('/faq', 'FrontController@faq');
Route::get('/contact-us', 'FrontController@contactus');
Route::post('/contact-us', 'FrontController@saveContact');

Route::get('/terms', 'FrontController@terms');
Route::get('/privacy', 'FrontController@privacy');

Route::post('/subscribe', 'FrontController@subscribe');
