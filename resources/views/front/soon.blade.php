<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="MRP" />

<!-- Stylesheets
============================================= -->
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="/css/style.css" type="text/css" />
<link rel="stylesheet" href="/css/dark.css" type="text/css" />
<link rel="stylesheet" href="/css/font-icons.css" type="text/css" />
<link rel="stylesheet" href="/css/animate.css" type="text/css" />
<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" />

<link rel="stylesheet" href="/css/responsive.css" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<!-- Document Title
============================================= -->
<title>Shamelist | A Comprehensive list of all corrupt public office holders in Nigeria | Coming Soon</title>

</head>

<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

<!-- Top Bar
============================================= -->
<div id="top-bar">

	<div class="container clearfix">

		<div class="col_half nobottommargin">

			<!-- Top Links
			============================================= -->
			<div class="top-links">
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">About</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div><!-- .top-links end -->

		</div>

		<div class="col_half fright col_last nobottommargin">

			<!-- Top Social
			============================================= -->
			<div id="top-social">
				<ul>
					<li><a href="#" class="si-facebook"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
					<li><a href="#" class="si-twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
					
					<li><a href="https://bitbucket.com/deparagon/shamelist" class="si-github"><span class="ts-icon"><i class="icon-github-circled"></i></span><span class="ts-text">Bitbucket</span></a></li>
					

					<li><a href="#" class="si-instagram"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
					

					<li><a href="mailto:info@shamelist.org.ng" class="si-email3"><span class="ts-icon"><i class="icon-envelope-alt"></i></span><span class="ts-text">info@shamelist.org.ng</span></a></li>
				</ul>
			</div><!-- #top-social end -->

		</div>

	</div>

</div><!-- #top-bar end -->

<!-- Header
============================================= -->
<header id="header" class="no-sticky">

	<div id="header-wrap">

		<div class="container clearfix">

			<!-- Logo
			============================================= -->
			<div id="logo" class="divcenter">
				<a href="{{ url('/') }}" class="standard-logo" data-dark-logo="/img/logo.png"><img class="divcenter" src="/img/logo.png" alt="ShameList"></a>
				<a href="{{ url('/') }}" class="retina-logo" data-dark-logo="/img/logo2.png"><img class="divcenter" src="/img/logo2.png" alt="ShameList"></a>
		</div><!-- #logo end -->

		</div>

	</div>

</header><!-- #header end -->

<!-- Content
============================================= -->
<section id="content">

	<div class="content-wrap">

		<div class="container subscribe-widget clearfix">

			<div class="heading-block title-center nobottomborder">
				<h1>ShameList is currently Under Construction</h1>
				<span>Lets Save Nigeria. Subscribe we will let you know when the site is ready</span>
			</div>

			<div id="countdown-ex1" class="countdown countdown-large divcenter bottommargin" style="max-width:700px;"></div>

			<div class="clear"></div>

			<div class="progress topmargin divcenter" style="height: 10px; max-width:600px;">
				<div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 25%">
					<span class="sr-only">25% Complete</span>
				</div>
			</div>

			<div class="divider divider-center divider-short divider-margin"><i class="icon-time"></i></div>

			<div id="subscribresult"></div>
			<form id="makesubscriptiongo" action="" role="form" method="post" class="nobottommargin">
				{{ csrf_field() }}
				<div class="input-group input-group-lg divcenter" style="max-width:600px;">
					<div class="input-group-prepend">
								<div class="input-group-text"><i class="icon-email2"></i></div>
							</div>
					<input type="email" id="maemail" name="email" class="form-control required email" placeholder="Enter your Email">
					<div class="input-group-append">
						<button class="btn btn-secondary" type="submit">Subscribe Now</button>
					</div>
				</div>
			</form>

		</div>

	</div>

</section><!-- #content end -->

<!-- Footer
============================================= -->
<footer id="footer" class="dark">

	<!-- Copyrights
	============================================= -->
	<div id="copyrights">

		<div class="container clearfix">

			<div class="col_half">
				Copyrights &copy; 2018 All Rights Reserved Shamelist.org.ng<br>
				<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
			</div>

			<div class="col_half col_last tright">
				<div class="fright clearfix">
					<a href="#" class="social-icon si-small si-borderless si-facebook">
						<i class="icon-facebook"></i>
						<i class="icon-facebook"></i>
					</a>

					<a href="#" class="social-icon si-small si-borderless si-twitter">
						<i class="icon-twitter"></i>
						<i class="icon-twitter"></i>
					</a>

					<a href="#" class="social-icon si-small si-borderless si-gplus">
						<i class="icon-gplus"></i>
						<i class="icon-gplus"></i>
					</a>


			

				</div>

				<div class="clear"></div>

				<i class="icon-envelope2"></i> info@shamelist.org.ng <span class="middot">&middot;</span> <i class="icon-headphones"></i> +2340 <span class="middot">&middot;</span> <i class="icon-skype2"></i> shamelist
			</div>

		</div>

	</div><!-- #copyrights end -->

</footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script src="/js/jquery.js"></script>
<script src="/js/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script src="/js/functions.js"></script>

<script src="/js/front.js"></script>

<script>
jQuery(document).ready( function($){
	var newDate = new Date(2018, 04, 1);
	$('#countdown-ex1').countdown({until: newDate});
});
</script>

</body>
</html>