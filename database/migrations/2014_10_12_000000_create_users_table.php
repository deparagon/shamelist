<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('phone')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->tinyInteger('staff')->default(0);
            $table->tinyInteger('power')->default(0);

            $table->string('facebookid')->nullable();
            $table->string('facebookname')->nullable();
            $table->string('facebooklink')->nullable();
            $table->string('facebookavatar')->nullable();

            $table->string('twitterid')->nullable();
            $table->string('twittername')->nullable();
            $table->string('twitterlink')->nullable();
            $table->string('twitteravatar')->nullable();
            $table->text('others')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
